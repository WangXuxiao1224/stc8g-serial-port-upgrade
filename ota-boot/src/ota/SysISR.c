#include "intrins.h"

#include <STC8G.H>
#include <EEPRom.h>

xdata	unsigned int recvlength=0;
xdata	unsigned int writelength=0;
xdata unsigned char databuff[600];
xdata unsigned char sendbuff[15];
xdata unsigned char recvtimeout=0;
xdata unsigned char datasendlength;
xdata unsigned char sendcnt=0;
bit RxLock=0;

extern xdata	unsigned int writeaddr;

void UART1_Isr(void)
{
//	if (TI)
//    {
//      TI = 0; 
//			if(sendcnt<datasendlength)
//			{
//				SBUF = sendbuff[sendcnt];
//				sendcnt++;
//			}
//    }
    if (RI)
    {
			RI = 0;
			if(0==RxLock)
			{
				recvtimeout=10;
				databuff[recvlength]=SBUF;
				recvlength++;
			}
    }
		
		#pragma asm
			RETI
		#pragma endasm	
		
}


void INT0_Routine(void) interrupt  0
{_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();}
void TM0_Rountine(void) interrupt  1
{_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();}
void INT1_Routine(void) interrupt  2
{_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();}
void TM1_Rountine(void) interrupt  3
{_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();}
void UART1_Routine(void) interrupt  4
{_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();}
void ADC_Routine(void) interrupt  5
{_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();}
void LVD_Routine(void) interrupt  6
{_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();}
void PCA_Routine(void) interrupt  7
{_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();}
void UART2_Routine(void) interrupt  8
{_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();}
void SPI_Routine(void) interrupt  9
{_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();}
void INT2_Routine(void) interrupt  10
{_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();}
void INT3_Routine(void) interrupt  11
{_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();}
void TM2_Routine(void) interrupt  12
{_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();}
void ISR_Interrupt_13(void) interrupt  13
{_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();}
void ISR_Interrupt_14(void) interrupt  14
{_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();}
void ISR_Interrupt_15(void) interrupt  15
{_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();}
void INT4_Routine(void) interrupt  16
{_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();}
void UART3_Routine(void) interrupt  17
{_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();}
void UART4_Routine(void) interrupt  18
{_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();}
void TM3_Routine(void) interrupt  19
{_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();}
void TM4_Routine(void) interrupt  20
{_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();}
void CMP_Routine(void) interrupt  21
{_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();}
void PWM0_Routine(void) interrupt  22
{_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();}
void PWM0FD_Routine(void) interrupt  23
{_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();}
void I2C_Routine(void) interrupt  24
{_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();}
void ISR_Interrupt_25(void) interrupt  25
{_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();}
void ISR_Interrupt_26(void) interrupt  26
{_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();}
void ISR_Interrupt_27(void) interrupt  27
{_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();}
void PWM1_Routine(void) interrupt  28
{_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();}
void PWM2_Routine(void) interrupt  29
{_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();}
void PWM3_Routine(void) interrupt  30
{_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();}
void PWM4_Routine(void) interrupt  31
{_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();}
