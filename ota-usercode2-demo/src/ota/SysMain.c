#include "intrins.h"

#include <STC8G.H>

sbit Led1		= P0^5;
sbit Led2		= P5^3;


void delayms(unsigned int dcnt)
{
    while(dcnt > 0) dcnt -- ;
}


void delay1s(unsigned int dcnt1)
{
    while(dcnt1 > 0)
    {
    delayms(5000);
    dcnt1 -- ;
    }
}


void main(void)
{
	
			P0M0 = 0x20;
			P0M1 = 0x00;
			P5M0 = 0x08;
			P5M1 = 0x00;
	
	while(1)
	{
		
							Led1=1;Led2=0;
				delay1s(250);
					Led1=0;Led2=1;				
					delay1s(250);	
		
	}
	
}